<?php

use Illuminate\Support\Facades\Route;
use App\Models\Categoria;

use App\Http\Controllers\ControllerPropiedades;
use App\Http\Controllers\PhotoController;
use App\Models\Propiedad;


Route::get('/', function () {
    //$propiedades = Propiedad::orderBy('id','DESC')->paginate(5);
    return view('home');
    //$data = Categoria::all();
    //return response()->json($data);
});

Route::get('/crearpropiedad',  [ControllerPropiedades::class, 'createPropiedad']);
Route::get('/listapropiedades',  [ControllerPropiedades::class, 'listPropiedades']);
Route::get('/propiedad/{slug}',  [ControllerPropiedades::class, 'detallePropiedad']);
Route::resource('/photos', PhotoController::class);

