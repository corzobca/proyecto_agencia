<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Propiedad;
use App\Models\Categoria;

class ControllerPropiedades extends Controller
{
    function createPropiedad(Request $request){

        //$data = Categoria::all();
        $propiedad = new Propiedad;
        $propiedad->titulo ="Departamentos Edificio Torre Empresarial";
        $propiedad->descripcion ="Hermosos departamentos en una zona exclusiva";
        $propiedad->precio =1345000.00;
        $propiedad->localizacion = "Centro,villahermosa";
        $propiedad->area = "450m2";
        $propiedad->cuartos = 2;
        $propiedad->baños = 4;
        $propiedad->garages = 2;
        $propiedad->cocinas = 0;
        $propiedad->puertas = 0;
        $propiedad->categoria_id = 1;
        if($propiedad->save()){
            $data = array(
                'status' => 'success',
                'message' => 'propiedad creada correctamente',
                'propiedad' => $propiedad
            );
        }else{
            $data = array(
                'status' => 'error',
                'message' => 'propiedad no fue creada correctamente'
            );

        }
        return response()->json($data);
    }

    function listPropiedades(Request $request){
        $data = Propiedad::paginate(4);
        return response()->json($data);
        }

    function detallePropiedad(Request $request, $slug){
        $propiedad = Propiedad::where('slug', $slug)->first();
        if(!$propiedad){
            return abort(404);
        }
        $categoria = Categoria::find($propiedad->categoria_id);
        // $imagenes = ImagenesPropiedad::where('propiedad_id', $propiedad->id)->get();
        //return response()->json($data);
        return view('detalle',['propiedad' => $propiedad,'categoria' => $categoria]);
    }
}
