<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Propiedad extends Model
{
    use HasFactory;
    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'titulo'
            ]
        ];
    }
    protected $table = 'propiedads';
    protected $appends = ['precio_formato'];

    public function categoria(){
        return $this->belongsTo(Categoria::class);
    }

    public function imagenes_propiedad(){
        return $this->hasMany(ImagenesPropiedad::class);
    }

    /*public function agente() {
        return $this->hasOne(Agente::class);
    }*/

    public function agente(){
        return $this->hasMany(Agente::class);
    }

    public function getPrecioFormatoAttribute(){
        return "$ ".$this->precio." MX";
    }
}
